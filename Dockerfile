FROM alpine

RUN apk --no-cache add \
	ansible \
	ansible-lint \
	openssh-client
